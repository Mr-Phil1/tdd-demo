import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;


public class KommentarTest {

    String kommentarText;
    String autor;
    int bewertung;
    Kommentar k1;

    @BeforeEach
    public void setup() {
        kommentarText = "Dies ist mein Kommentartext";
        autor = "Claudio Landerer";
        bewertung = 3;
        k1 = new Kommentar(kommentarText, autor, bewertung);
    }

    @Test
    public void testAnlegen_korrekteInputParameter_kommentarAngelget() {
        // Given siehe setup-Methode
        String kommentarText = "Dies ist mein Kommentartext";
        String autor = "Claudio Landerer";
        int bewertung = 3;
        // When
        Kommentar k1 = new Kommentar(kommentarText, autor, bewertung);
        // Then
        Assertions.assertEquals("Dies ist mein Kommentartext", k1.getKommentarText());
        Assertions.assertEquals(0, k1.getStimmen());
        Assertions.assertEquals(3, k1.getBewertung());
        Assertions.assertEquals("Claudio Landerer", k1.getAutor());
    }

    @Test
    public void testAnlegen_falscheInputParameter_kommentarAngelget() {
        // Given
        String kommentarText = "Dies ist mein Kommentartext";
        String autor = "Claudio Landerer";
        int bewertung = 3;

        //When Then
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(null, autor, bewertung));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(kommentarText, null, bewertung));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(kommentarText, autor, 0));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(kommentarText, autor, 6));
    }

    @Test
    public void testAnlegen_falscheInputParameter_kommentarAngelget_v2() {
        // Given
        String kommentarText = "Dies ist mein Kommentartext";
        String autor = "Claudio Landerer";
        int bewertung = 3;
        ArrayList<Throwable> al = new ArrayList<>();
        try {
            Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(kommentarText, autor, bewertung));
        } catch (AssertionError e1) {
            al.add(e1);
        }
        try {
            Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(kommentarText, autor, bewertung));
        } catch (AssertionError e2) {
            al.add(e2);
        }
        try {
            Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(kommentarText, autor, 0));
        } catch (AssertionError e3) {
            al.add(e3);
        }
        try {
            Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(kommentarText, autor, 6));
        } catch (AssertionError e4) {
            al.add(e4);
        }

        if (al.size() > 0) {
            System.out.println("Ausgabe");
            System.out.println(al);
            Assertions.fail();
        }
    }

    @Test
    public void test_testkommentar_anlegen() {
        // Given siehe Setup

        // When

        // Then
        Assertions.assertEquals(0, k1.getStimmen());

        // When
        k1.zustimmen();

        //Then
        Assertions.assertEquals(1, k1.getStimmen());

    }
    @AfterEach
    public void afterEachTest(){
        System.out.println("ssss");
    }
}
